//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" ", "~/.local/bin/statusbar/sb-cpu", 5, 0},
	{"", "~/.local/bin/statusbar/sb-memory",	5,		0},
	{"", "~/.local/bin/statusbar/sb-brightness", 0, 10},
	{"", "~/.local/bin/statusbar/sb-audio", 0, 10},
	{"", "~/.local/bin/statusbar/sb-battery", 10, 0},
	{"", "~/.local/bin/statusbar/sb-internet",10,0},
	{"", "~/.local/bin/statusbar/sb-clock", 10,0},
//	{"", "~/.local/bin/statusbar/sb-test", 1,0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
